Document: node-pretty-hrtime
Title: Debian node-pretty-hrtime Manual
Author: <insert document author here>
Abstract: This manual describes what node-pretty-hrtime is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/node-pretty-hrtime/node-pretty-hrtime.sgml.gz

Format: postscript
Files: /usr/share/doc/node-pretty-hrtime/node-pretty-hrtime.ps.gz

Format: text
Files: /usr/share/doc/node-pretty-hrtime/node-pretty-hrtime.text.gz

Format: HTML
Index: /usr/share/doc/node-pretty-hrtime/html/index.html
Files: /usr/share/doc/node-pretty-hrtime/html/*.html
